﻿using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour 
{

	public Animator m_Animator;

	private Color m_Color;
	public SpriteRenderer m_SpriteRenderer;

	public bool m_IsRunning, m_IsCharging, m_IsJumping;

	// Use this for initialization
	void Start () 
	{
		m_Animator = GetComponent<Animator>();
		m_SpriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		m_Animator.SetBool("IsRunning" , m_IsRunning);
		m_Animator.SetBool("IsCharging", m_IsCharging);
		m_Animator.SetBool("IsJumping" , m_IsJumping);	
		m_SpriteRenderer.color = m_Color;
		

	}

	public void GiveColor(Color _color)
	{
		m_Color = _color;
	}

	public void SetScale(float _sign)
	{
		Vector3 scale = transform.localScale;
		if(_sign < 0 && scale.x > 0)
		{
			scale.x = -scale.x;
		}

		if(_sign > 0 && scale.x < 0)
		{
			scale.x = -scale.x ;
		}
		transform.localScale = scale;
	}


}
