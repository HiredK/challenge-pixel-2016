﻿using UnityEngine;
using System.Collections;

public abstract class Entity : MonoBehaviour
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public enum State
    {
        Sleeping, // When the entity is not activated
        Idle,     // When the entity is not moving
        Dead      // When the entity is dead
    };

    public State CurrentState { get; set; }

    [System.Serializable]
    public class Ghosting
    {
        public GameObject m_GhostPrefab;
        public Color m_GhostColor = Color.white;
        public float m_GhostSpawnDelay = 0.08f;
        public float m_GhostLifeSpan = 0.25f;
        public bool m_EnableGhosting = true;
    }

    public Ghosting m_Ghosting = new Ghosting();
    SpriteRenderer m_Renderer;

    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    protected virtual void Start()
    {
        m_Renderer = GetComponentInChildren<SpriteRenderer>();
        CurrentState = State.Sleeping;
        StartSpawnGhost();
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Privates:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    private IEnumerator SpawnGhost(float delay)
    {
        if (m_Ghosting.m_GhostPrefab)
        {
            GameObject ghost = GameObject.Instantiate(m_Ghosting.m_GhostPrefab, transform.position, transform.rotation) as GameObject;
            ghost.GetComponent<SpriteRenderer>().sprite = m_Renderer.sprite;
            ghost.GetComponent<SpriteRenderer>().color = m_Ghosting.m_GhostColor;
            ghost.transform.localScale = transform.localScale;
            Destroy(ghost, m_Ghosting.m_GhostLifeSpan);
        }

        yield return new WaitForSeconds(delay);
        StartSpawnGhost();
    }

    public void StartSpawnGhost()
    {
        if (m_Ghosting.m_EnableGhosting) {
            StartCoroutine(SpawnGhost(m_Ghosting.m_GhostSpawnDelay));
        }
    }
}