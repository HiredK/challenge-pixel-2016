﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour 
{
	public bool m_IsActive = true;

	// Use this for initialization
	void Start () 
	{
		Physics2D.IgnoreLayerCollision(8,8);
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	void OnCollisionEnter2D(Collision2D _col)
	{
        if (_col.gameObject.tag == "Wall")
		{
			m_IsActive = false;
			GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            GetComponent<BoxCollider2D>().isTrigger = true;
		}	
	}
}