﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public int m_InitialPlayerID;

    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    void Update()
    {
        for (int i = 0; i < GameManager.GetInstance().GetCurrentPlayerCount(); ++i)
        {
            Player player = GameManager.GetInstance().GetPlayerAt(i);   

         

            if (player.CurrentState == Entity.State.Sleeping)
            {
                if (m_InitialPlayerID == player.ID)
                {
                    player.transform.position = new Vector3(
                        transform.position.x,
                        transform.position.y,
                        player.transform.position.z
                    );

                    player.CurrentState = Entity.State.Idle;

                }
            }
        }
    }

#if UNITY_EDITOR
    /*----------------------------------------------------------------------------*/
    void OnDrawGizmos()
    {
        Gizmos.color = GetColorFromPlayerID(m_InitialPlayerID);
        Gizmos.DrawCube(transform.position, new Vector3(transform.localScale.x, transform.localScale.y, 0));
        Gizmos.color = Color.white;
    }
#endif

    ////////////////////////////////////////////////////////////////////////////////
    // Privates:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    static Color GetColorFromPlayerID(int player_id)
    {
        switch (player_id)
        {
            case 0:
                return new Color(1.0f, 0.0f, 0.0f, 0.5f);
            case 1:
                return new Color(0.0f, 1.0f, 0.0f, 0.5f);
            case 2:
                return new Color(0.0f, 0.0f, 1.0f, 0.5f);
            case 3:
                return new Color(1.0f, 1.0f, 0.0f, 0.5f);
            default:
                Debug.LogError("Invalid player id!");
                break;
        }

        return Color.white;
    }

    private Color GivePlayerColor(int _index)
    {

    	Color color = Color.white;
		switch (_index) 
		{
			case 0:
				color = new Color(1, 64/255f, 64/255f);
				break;
			case 1:
				color = new Color(64/255f, 1 , 64/255f);				
				break;
			case 2:
				color = new Color(64/255f, 64/255f , 1);				
				break;
			case 3:
				color = new Color(1,1,64/255f);					
				break;
			default:
				break;
		}

		return color;
    }

}