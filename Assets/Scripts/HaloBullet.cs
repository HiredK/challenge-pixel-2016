﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HaloBullet : MonoBehaviour 
{
	public GameObject m_BulletBetweenPrefab;

	public GameObject[] m_BulletList;
	public int m_BulletListIndex = 0;
	public int m_BulletListNumber = 0;

	public Vector3 m_InitPosition;

	private Node[] m_NodeList;

	public float m_LifeTime = 0;

	public bool[] m_PlayerHit = {false, false, false, false};


	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		UpdateNode();

	}
	public void CreateList()
	{
		m_BulletList = new GameObject[m_BulletListNumber];
	}

	public void UpdateNode()
	{
		for(int i = 0; i < m_NodeList.Length; i++)
		{
			m_NodeList[i].SetPosition();
			if(Vector3.Distance(m_InitPosition, m_NodeList[i].bulletBetween.transform.position) > 30)
			{
				Destroy(gameObject);
			}

			if(!m_NodeList[i].left.GetComponent<Bullet>().m_IsActive || !m_NodeList[i].right.GetComponent<Bullet>().m_IsActive)
			{
				m_NodeList[i].bulletBetween.GetComponent<MeshRenderer>().enabled = false;
			}

            if (m_NodeList[i].bulletBetween.transform.localScale.x <= 0.02f)
            {
                Destroy(gameObject);
            }
		}
	}


	public void UpdatePlayerHitList(int _index)
	{
		m_PlayerHit[_index] = true;
	}

	public class Node
	{
		public GameObject left = null;
		public GameObject right = null;

		public GameObject bulletBetween;

		private float lifeTime;

		public Node(GameObject _left, GameObject _right, GameObject _bullet, float _lifeTime, Color _color)
		{
			left = _left;
			right = _right;


			//TO SET START SIZE
			lifeTime = _lifeTime*2;



			float disX = (right.transform.position.x - left.transform.position.x)/2; 
			float disY = (right.transform.position.y - left.transform.position.y)/2; 
			Vector3 pos = new Vector3(_left.transform.position.x + disX, _left.transform.position.y + disY,-1);
			bulletBetween = Instantiate(_bullet, pos, Quaternion.identity) as GameObject;
			Vector3 scale = bulletBetween.transform.localScale;
			scale.x = lifeTime/10;
			bulletBetween.transform.localScale = scale;
            bulletBetween.GetComponent<BulletBetween>().m_Mat.color = _color;
		}

		public void SetPosition()
		{
			float disX = (right.transform.position.x - left.transform.position.x)/2; 
			float disY = (right.transform.position.y - left.transform.position.y)/2; 
			Vector3 pos = new Vector3(left.transform.position.x + disX, left.transform.position.y + disY, -1);

			bulletBetween.transform.position = pos;

			//watching direction
			bulletBetween.transform.up = (right.transform.position - bulletBetween.transform.position).normalized;

			//set the width
			Vector3 scale = bulletBetween.transform.localScale;
			scale.y = Vector3.Distance(left.transform.position,right.transform.position);		
			bulletBetween.transform.localScale = scale;


		
		}

	}

	public void CreateBetween(Vector3 _initPos, GameObject _parent, Color _color)
	{
		m_NodeList = new Node[m_BulletList.Length];

		for(int i = 0; i < m_NodeList.Length; i++)
		{
			Node node = null;
			if(i == m_BulletList.Length-1)
			{
				node = new Node(m_BulletList[m_BulletList.Length-1], m_BulletList[0], m_BulletBetweenPrefab, m_LifeTime, _color);
			}
			else
			{
				node = new Node(m_BulletList[i], m_BulletList[i+1], m_BulletBetweenPrefab, m_LifeTime, _color);
			}
			m_NodeList[i] = node;
			m_NodeList[i].bulletBetween.transform.parent = _parent.transform;
			m_NodeList[i].bulletBetween.GetComponent<BulletBetween>().SetHaloParent(gameObject);
		}


		m_InitPosition = _initPos;
	}


}
