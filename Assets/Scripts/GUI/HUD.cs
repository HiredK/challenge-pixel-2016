﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUD : MonoBehaviour
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public FadeOverlay m_FadeOverlay;
    public GameObject m_HUDMenu;

    public bool m_GoBackToMenu;

    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    void Start()
    {
        m_HUDMenu.SetActive(false);
        m_GoBackToMenu = false;
    }

    /*----------------------------------------------------------------------------*/
    void Update()
    {
        if (GameManager.GetInstance().GetCurrentPlayerCount() > 0)
        {
            Player active = GameManager.GetInstance().GetPlayerAt(0);
            if (active.Device.MenuWasPressed)
            {
                if (!m_HUDMenu.activeSelf)
                {
                    m_HUDMenu.SetActive(true);
                }
                else
                {
                    m_HUDMenu.SetActive(false);
                }
            }

            if (active.Device.Action1)
            {
                if (m_HUDMenu.activeSelf)
                {
                    m_FadeOverlay.m_PerformFadeOut = true;
                    m_GoBackToMenu = true;
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Accessors:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public bool IsDone()
    {
        return m_FadeOverlay.IsDone() && m_GoBackToMenu;
    }

    /*----------------------------------------------------------------------------*/
    public bool IsOpen()
    {
        return m_HUDMenu.activeSelf;
    }
}