﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ReadyButton : MonoBehaviour 
{
	public Sprite m_Ready;
	public Sprite m_NotReady;

	private Image m_Image;

	// Use this for initialization
	void Start () 
	{
		m_Image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}


	public void SetReadyButton(bool _ready)
	{
		if(_ready)
		{
			m_Image.sprite = m_Ready;
		}
		else
		{
			m_Image.sprite = m_NotReady;		
		}
	}



}
