﻿using UnityEngine;
using System.Collections;

public class CharacterSelect : MonoBehaviour 
{
	private bool m_IsDone = false;
	private int m_NumOfActivePlayer = 0;
	private bool m_PanelShown = false;

	public Sprite[] m_PlayerSpriteList;
	public Color[] m_PlayerColorList;
	public GameObject[] m_PanelList;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(m_PanelShown)
		{
			m_IsDone = (m_NumOfActivePlayer == NumOfReadyPlayer());
		}
	}

	public bool IsDone()
	{
		if(m_IsDone)
		{
			for (int i = 0; i < GameManager.GetInstance().GetCurrentPlayerCount(); ++i)
        	{
           		Player player = GameManager.GetInstance().GetPlayerAt(i);
				player.ResetPlayerLife(); 
				player.m_Sprite.color = GivePlayerColor(m_PanelList[i].GetComponent<Panel>().m_ColorSlot);    //OUIN AYUSSI 

				GameManager.GetInstance().GiveAnimationToPlayer(player.transform.gameObject, m_PanelList[i].GetComponent<Panel>().m_SpriteSlot, player.m_Sprite.color); // VOIR
				player.SetPlayerAnimation();   		  
           	}

            SoundManager.SPEAKER.PlaySound("MenuMove");
		}

		return m_IsDone;
	}

	public void SetNumOfActivePlayer(int _num)
	{
		m_NumOfActivePlayer = _num;
	}

	public void CreatePanel()
	{
		for(int i = 0; i < m_NumOfActivePlayer; i++)
		{
			m_PanelList[i].SetActive(true);
		}
		m_PanelShown = true;
	}

	private int NumOfReadyPlayer()
	{
		int num = 0;
		for(int i = 0; i < m_PanelList.Length; i++)
		{			
			if(m_PanelList[i].activeSelf)
			{
				if(m_PanelList[i].GetComponent<Panel>().m_PlayerLocked)
				{
					num++;
				}
			}
		}
		return num;
	}

	private Color GivePlayerColor(int _index)
    {

    	Color color = Color.white;
		switch (_index) 
		{
			case 0:
				color = new Color(1, 64/255f, 64/255f);
				break;
			case 1:
				color = new Color(64/255f, 1 , 64/255f);				
				break;
			case 2:
				color = new Color(64/255f, 64/255f , 1);				
				break;
			case 3:
				color = new Color(1,1,64/255f);					
				break;
			default:
				break;
		}

		return color;
    }





}
