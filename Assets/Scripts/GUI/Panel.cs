﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Panel : MonoBehaviour 
{
	public Image m_Sprite;
	public Color m_Color;
	public int m_PlayerId = 0;

	public int m_SpriteSlot = 0;
	public int m_ColorSlot = 0;

	public GameObject m_CharacterSelectObj;
	private CharacterSelect m_CharacterSelect;

	public int m_Cursor = 0;
	public bool m_PlayerLocked = false;

	public GameObject m_Arrow;
	private Vector2 m_Cursor0Pos = new Vector2(0,15);
	private Vector2 m_Cursor1Pos = new Vector2(0,-70);

	public GameObject m_ColorButton;
	private Image m_SpriteImage;

	public GameObject m_ReadyButtonObj;
	private ReadyButton m_ReadyButton;

	// Use this for initialization
	void Start () 
	{
		m_CharacterSelect = m_CharacterSelectObj.GetComponent<CharacterSelect>();
		m_SpriteImage = m_ColorButton.GetComponent<Image>();
		m_ReadyButton = m_ReadyButtonObj.GetComponent<ReadyButton>();
		m_ColorSlot = m_SpriteSlot = m_PlayerId;
		m_Sprite.sprite = m_CharacterSelect.m_PlayerSpriteList[m_SpriteSlot];
	}
	
	// Update is called once per frame
	void Update () 
	{
		m_Color = m_CharacterSelect.m_PlayerColorList[m_ColorSlot];
		m_Sprite.color = m_Color;
		m_SpriteImage.color = m_Color;
		m_ReadyButton.SetReadyButton(m_PlayerLocked);

		if(!m_PlayerLocked)
		{
			if(GameManager.GetInstance().GetPlayerAt(m_PlayerId))
			{
				if(m_Cursor == 0)
				{
					if(GameManager.GetInstance().GetPlayerAt(m_PlayerId).Device.DPadRight.WasPressed)
					{
                        SoundManager.SPEAKER.PlaySound("MenuMove");
						SetSpriteSlot("right");
					}
					if(GameManager.GetInstance().GetPlayerAt(m_PlayerId).Device.DPadLeft.WasPressed)
					{
                        SoundManager.SPEAKER.PlaySound("MenuMove");
						SetSpriteSlot("left");
					}
					if(GameManager.GetInstance().GetPlayerAt(m_PlayerId).Device.DPadUp.WasPressed ||
					 GameManager.GetInstance().GetPlayerAt(m_PlayerId).Device.DPadDown.WasPressed)
					{
                        SoundManager.SPEAKER.PlaySound("MenuMove");
						m_Cursor = 1;
					}

				}
				else
				{
					if(GameManager.GetInstance().GetPlayerAt(m_PlayerId).Device.DPadRight.WasPressed)
					{
                        SoundManager.SPEAKER.PlaySound("MenuMove");
						SetColorSlot("right");
					}
					if(GameManager.GetInstance().GetPlayerAt(m_PlayerId).Device.DPadLeft.WasPressed)
					{
                        SoundManager.SPEAKER.PlaySound("MenuMove");
						SetColorSlot("left");
					}
					if(GameManager.GetInstance().GetPlayerAt(m_PlayerId).Device.DPadUp.WasPressed ||
					 GameManager.GetInstance().GetPlayerAt(m_PlayerId).Device.DPadDown.WasPressed)
					{
                        SoundManager.SPEAKER.PlaySound("MenuMove");
						m_Cursor = 0;
					}
				}			
			}
			if(GameManager.GetInstance().GetPlayerAt(m_PlayerId).Device.Action1.WasPressed)
			{
				m_PlayerLocked = true;
			}
		}

		if(m_Cursor == 0)
		{
			m_Arrow.GetComponent<RectTransform>().localPosition = m_Cursor0Pos;
		}
		else
		{
			m_Arrow.GetComponent<RectTransform>().localPosition = m_Cursor1Pos;
		}

	}

	public void SetSpriteSlot(string _dir)
	{
		if(_dir == "right")
		{
			if(m_SpriteSlot == m_CharacterSelect.m_PlayerSpriteList.Length-1)
			{
				m_SpriteSlot = 0;
			}
			else
			{
				m_SpriteSlot++;
			}
		}
		else
		{
			if(m_SpriteSlot == 0)
			{
				m_SpriteSlot = m_CharacterSelect.m_PlayerSpriteList.Length-1;
			}
			else
			{
				m_SpriteSlot--;
			}
		}

		m_Sprite.sprite = m_CharacterSelect.m_PlayerSpriteList[m_SpriteSlot];
	}


	public void SetColorSlot(string _dir)
	{
		if(_dir == "right")
		{
			if(m_ColorSlot == m_CharacterSelect.m_PlayerColorList.Length-1)
			{
				m_ColorSlot = 0;
			}
			else
			{
				m_ColorSlot++;
			}
		}
		else
		{
			if(m_ColorSlot == 0)
			{
				m_ColorSlot = m_CharacterSelect.m_PlayerColorList.Length-1;
			}
			else
			{
				m_ColorSlot--;
			}
		}

		m_Color = m_CharacterSelect.m_PlayerColorList[m_ColorSlot];
		//m_Sprite.color = m_CharacterSelect.m_PlayerColorList[m_ColorSlot];
	}

}
