﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResultScreen : MonoBehaviour
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public FadeOverlay m_FadeOverlay;

    public PlayerScore m_Player0_Score;
    public PlayerScore m_Player1_Score;
    public PlayerScore m_Player2_Score;
    public PlayerScore m_Player3_Score;

    private CanvasGroup m_CanvasGroup;
    private bool m_ShowPanel;
    private bool m_NeedIncrementScore;

    public bool m_GoToMenu;

    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    void Start()
    {
        m_CanvasGroup = GetComponent<CanvasGroup>();
        m_CanvasGroup.alpha = 0.0f;
        m_ShowPanel = false;
        m_NeedIncrementScore = false;

        m_GoToMenu = false;

        Refresh();
    }

    /*----------------------------------------------------------------------------*/
    void Update()
    {
        if (m_ShowPanel) {
            m_CanvasGroup.alpha = Mathf.Lerp(m_CanvasGroup.alpha, 1.0f, Time.deltaTime * 5.0f);
            if (m_NeedIncrementScore == false)
            {
                StartCoroutine(ShowScore(2.0f));
                m_NeedIncrementScore = true;
            }
        }
        else {
            m_CanvasGroup.alpha = Mathf.Lerp(m_CanvasGroup.alpha, 0.0f, Time.deltaTime * 8.0f);
        }
    }

    IEnumerator ShowScore(float duration)
    {
        yield return new WaitForSeconds(duration * 0.5f);
        Refresh();
        yield return new WaitForSeconds(duration * 0.5f);
        GameManager.GetInstance().ResetPlayers();
        m_NeedIncrementScore = false;

        int round_result = PlayerWon();
        if (round_result != -1) {
            m_FadeOverlay.SetFadeOut();
            m_GoToMenu = true;
        }
        else {
            Hide();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Accessors:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public bool IsDone() {
        return m_FadeOverlay.IsDone() && m_GoToMenu == true;
    }
    /*----------------------------------------------------------------------------*/
    public void Show() {
        m_ShowPanel = true;
    }
    /*----------------------------------------------------------------------------*/
    public void Hide() {
        m_ShowPanel = false;
    }
    /*----------------------------------------------------------------------------*/
    public bool IsShowing() {
        return m_ShowPanel;
    }

    /*----------------------------------------------------------------------------*/
    public void Refresh()
    {
        if (GameManager.GetInstance().GetCurrentPlayerCount() >= 1) {
            m_Player0_Score.Refresh(GameManager.GetInstance().GetPlayerAt(0));
        }
        if (GameManager.GetInstance().GetCurrentPlayerCount() >= 2) {
            m_Player1_Score.Refresh(GameManager.GetInstance().GetPlayerAt(1));
        }
        if (GameManager.GetInstance().GetCurrentPlayerCount() >= 3) {
            m_Player2_Score.Refresh(GameManager.GetInstance().GetPlayerAt(2));
        }
        if (GameManager.GetInstance().GetCurrentPlayerCount() >= 4) {
            m_Player3_Score.Refresh(GameManager.GetInstance().GetPlayerAt(3));
        }
    }

    public int PlayerWon()
    {
        if (GameManager.GetInstance().GetCurrentPlayerCount() >= 1) {
            if (m_Player0_Score.HasPlayerWon(GameManager.GetInstance().GetPlayerAt(0))) {
                return 0;
            }
        }
        if (GameManager.GetInstance().GetCurrentPlayerCount() >= 2) {
            if (m_Player1_Score.HasPlayerWon(GameManager.GetInstance().GetPlayerAt(1))) {
                return 1;
            }
        }
        if (GameManager.GetInstance().GetCurrentPlayerCount() >= 3) {
            if (m_Player2_Score.HasPlayerWon(GameManager.GetInstance().GetPlayerAt(2))) {
                return 2;
            }
        }
        if (GameManager.GetInstance().GetCurrentPlayerCount() >= 4) {
            if (m_Player3_Score.HasPlayerWon(GameManager.GetInstance().GetPlayerAt(3))) {
                return 3;
            }
        }

        return -1;
    }
}