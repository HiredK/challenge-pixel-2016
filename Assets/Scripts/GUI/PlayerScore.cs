﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class PlayerScore : MonoBehaviour
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    private const int MAX_ITEMS__ = 12;
    private List<GameObject> m_items = new List<GameObject>(MAX_ITEMS__);

    public GameObject m_ScoreItem;
    public int m_PlayerID;

    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    void Start()
    {
        m_ScoreItem.SetActive(true);
        m_items.Add(m_ScoreItem);

        float lastX = m_ScoreItem.GetComponent<RectTransform>().localPosition.x;
        for (int i = 0; i < GameManager.GetInstance().m_NumberOfRound-1; ++i)
        {
            GameObject next = GameObject.Instantiate(m_ScoreItem);
            next.transform.position = new Vector3(
                lastX + (32.0f + 6.0f),
                m_ScoreItem.transform.position.y,
                m_ScoreItem.transform.position.z
            );

            next.transform.SetParent(m_ScoreItem.transform.parent, false);
            lastX = next.GetComponent<RectTransform>().localPosition.x;
            m_items.Add(next);
        }
    }

    /*----------------------------------------------------------------------------*/
    public void Refresh(Player player)
    {
        if (player.ID == m_PlayerID)
        {
            for (int i = 0; i < GameManager.GetInstance().m_NumberOfRound; ++i)
            {
                if (i < player.Score) {
                    GameObject item = m_items[i];
                    item.GetComponent<Image>().color = Color.green;
                }
            }
        }
    }

    public bool HasPlayerWon(Player player)
    {
        if (player.ID == m_PlayerID)
        {
            if (player.Score == GameManager.GetInstance().m_NumberOfRound) {
                return true;
            }
        }

        return false;
    }
}