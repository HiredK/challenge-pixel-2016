﻿using UnityEngine;
using System.Collections;
using InControl;

public class Player : Entity
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public InputDevice Device { get; set; }
    public int ID { get; set; }
    public int Score { get; set; }

    private Rigidbody2D m_RigidBody;
    public SpriteRenderer m_Sprite;

    const float JUMP_HEIGHT_LOW__ = 0.1f;
    const float JUMP_HEIGHT_HIGH__ = 3.0f;
    private float m_JumpHeight;
    private float m_CurrentHeight;

    public bool m_Jumping;
	public bool m_Slaming;

    const float SPEED_LOW__ = 5.0f;
    const float SPEED_HIGH__ = 25.0f;
    private float m_TargetSpeed = JUMP_HEIGHT_LOW__;
    private float m_Speed;

    private float m_SpeedRatio;

    public GameObject m_ShadowPrefab;
    GameObject m_ShadowInstance;
    private Vector3 m_ShadowPos;

    private Shockwave m_Shockwave;
    public GameObject m_SlamImpactPrefab;

    public bool m_OutsideOfBounds;

    //GOO STUFF
	private float m_Life = 100;
    private float m_MaxLife = 100;
	private float m_BleedCounter = 10;
	private float m_BleedCounterInit = 10;
	private GooEmittor m_GooEmittor;
	private float m_InitScale;
	private float m_MinScale;

	//ANIMATION STUFF
	private PlayerAnimation m_PlayerAnimation;
	private bool m_IsAnimationSet = false;
	private bool m_IsChargingAnimation = false;
	private bool m_IsRunningAnimation = false;
	//I USE M_JUMPING FOR THE JUMP

    private bool m_GettingHurt;

    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    protected override void Start()
    {
        base.Start();

        m_RigidBody = GetComponent<Rigidbody2D>();
        m_RigidBody.freezeRotation = true;

        m_Shockwave = GetComponent<Shockwave>();
		m_GooEmittor = GetComponent<GooEmittor>();
		m_Sprite = GetComponent<SpriteRenderer>();
		m_InitScale = transform.localScale.x;
		m_MinScale = m_InitScale/2;

        m_ShadowInstance = Instantiate(m_ShadowPrefab);
        GameObject.DontDestroyOnLoad(m_ShadowInstance);
        m_ShadowInstance.SetActive(false);

        m_JumpHeight = JUMP_HEIGHT_LOW__;
        m_Jumping = false;
        m_Slaming = false;

        m_CurrentHeight = 0.0f;
        m_TargetSpeed = SPEED_HIGH__;
        m_Speed = SPEED_HIGH__;
        m_SpeedRatio = 1.0f;

        m_OutsideOfBounds = false;
    }

    /*----------------------------------------------------------------------------*/
    void Update()
    {
		UpdateAnimation();

        if (m_OutsideOfBounds && !m_Jumping && !m_Slaming)
        {
            Kill();
            return;
        }

		m_ShadowInstance.SetActive(true);
		m_ShadowInstance.transform.position = m_ShadowPos;

		if(!m_Jumping ||! m_Slaming)
		{
			m_ShadowPos = transform.position;
		}

        if (m_Jumping || m_Slaming)
        {   
			if(m_PlayerAnimation)
            	m_PlayerAnimation.m_SpriteRenderer.sortingOrder = 3;
        }
        else
        {   
			if(m_PlayerAnimation)  
			{
				if(m_PlayerAnimation.m_SpriteRenderer)
				{
					m_PlayerAnimation.m_SpriteRenderer.sortingOrder = 1; 
				}
			}   
				           
        }

        if (GameManager.GetInstance().isPlayersInputLocked()) {
            m_RigidBody.constraints = RigidbodyConstraints2D.FreezeAll;
            return;
        }
        else {
            m_RigidBody.constraints = RigidbodyConstraints2D.FreezeRotation;
        }

        if (CurrentState != Entity.State.Sleeping)
        {
            if (!m_Jumping)
            {
                if (Device.Action1.WasPressed)
                {
                    m_TargetSpeed = SPEED_LOW__;
                }

                if (Device.Action1.IsPressed)
                {
                    if (m_JumpHeight < JUMP_HEIGHT_HIGH__)
                    {
                        m_JumpHeight += Time.deltaTime * 5.0f;
                    }

                    if(m_PlayerAnimation != null)
                    {
						m_PlayerAnimation.m_IsCharging = true;
                    }		
								
                }
                else
                {
					if(m_PlayerAnimation != null)
                    {
						m_PlayerAnimation.m_IsCharging = false;
                    }	
					
                }

                if (Device.Action1.WasReleased)
                {
                    Vector2 dest = new Vector2(
                        transform.position.x + (Device.LeftStick.Vector.x * 2.5f),
                        transform.position.y + (Device.LeftStick.Vector.y * 2.5f)
                    );

                    StartCoroutine(Jump(new Vector3(dest.x, dest.y, transform.position.z), 0.8f));
                    m_TargetSpeed = SPEED_HIGH__;
                }
            }

            if (Device.Action3.WasPressed && m_Jumping && !m_Slaming)
            {
                Vector2 dest = new Vector2(
                    transform.position.x,
                    transform.position.y - m_CurrentHeight
                );

                StartCoroutine(Slam(new Vector3(dest.x, dest.y, transform.position.z), 0.1f));
            }

            m_Speed = Mathf.Lerp(m_Speed, m_TargetSpeed, Time.deltaTime * 10.0f);

            m_RigidBody.AddForce(Device.LeftStick.Vector * (m_Speed + (25.0f * m_SpeedRatio)));

            // FOR ANIMATION
			if(Device.LeftStick.IsPressed)			
			{
				m_PlayerAnimation.m_IsRunning = true;
				if(Device.LeftStick.Vector.x != 0)
				{
					m_PlayerAnimation.SetScale(Mathf.Sign(Device.LeftStick.Vector.x));
				}
			}
			else			
			{
				m_PlayerAnimation.m_IsRunning = false;

			}

            AdjustBodyVelocity(0.8f);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Privates:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    private void AdjustBodyVelocity(float factor)
    {
        m_RigidBody.velocity = new Vector2(
            m_RigidBody.velocity.x * factor,
            m_RigidBody.velocity.y * factor
        );
    }

    /*----------------------------------------------------------------------------*/
    IEnumerator Jump(Vector3 dest, float time)
    {
        if (m_Jumping)
        {
            yield break;
        }

        Debug.Log("Jump coroutine start");

        m_Jumping = true;
        var startPos = transform.position;
        var timer = 0.0f;

        while (timer <= 1.0f && m_Slaming == false) {
            m_CurrentHeight = Mathf.Sin(Mathf.PI * timer) * m_JumpHeight;
            Vector3 origin = Vector3.Lerp(startPos, dest, timer);
            m_ShadowPos = origin;

            transform.position = origin + Vector3.up * m_CurrentHeight;
            timer += Time.deltaTime / time;

            yield return null;
        }

        m_JumpHeight = JUMP_HEIGHT_LOW__;
        m_Jumping = false;
		LoseLife(2, false, "jumping");

        if (m_OutsideOfBounds && !m_Slaming) {
            SoundManager.SPEAKER.PlaySound("DeathWater");
            Kill();
        }
    }

    IEnumerator Slam(Vector3 dest, float time)
    {
        if (m_Slaming && CurrentState != State.Sleeping)
        {
            yield break;
        }

        m_Ghosting.m_EnableGhosting = true;
        StartSpawnGhost();

        m_Slaming = true;
        var startPos = transform.position;
        var timer = 0.0f;

        while (timer <= 1.0f) {
            Vector3 origin = Vector3.Lerp(startPos, dest, timer);
            m_ShadowPos = origin;

            transform.position = origin;
            timer += Time.deltaTime / time;

            yield return null;
        }

        m_Ghosting.m_EnableGhosting = false;
        m_JumpHeight = JUMP_HEIGHT_LOW__;

        if (m_OutsideOfBounds && m_Slaming) {
            SoundManager.SPEAKER.PlaySound("DeathWater");
            m_Slaming = false;
            Kill();
        }
        else {
            if (m_CurrentHeight > (JUMP_HEIGHT_HIGH__ - 0.3f)) {
                GameManager.GetInstance().PerformRippleEffect(transform.position);
                GameManager.GetInstance().PerformScreenShake(0.25f);
                CreateSlamImpactPrefab(25.0f);

                SoundManager.SPEAKER.PlaySound("SuperSlam");
            }
            else
            {
                SoundManager.SPEAKER.PlaySound("Slam");
            }

            m_Shockwave.DoShockwave(gameObject, m_CurrentHeight, m_Sprite.color);
			LoseLife(3, false, "by slamming");
			SlamDamage();
        }

        m_Slaming = false;
    }

    /*----------------------------------------------------------------------------*/
    void CreateSlamImpactPrefab(float lifetime)
    {
        GameObject impact = Instantiate(m_SlamImpactPrefab);

        impact.transform.position = new Vector3(
            transform.position.x,
            transform.position.y,
            impact.transform.position.z
        );

        impact.transform.Rotate(0.0f, 0.0f, Random.RandomRange(0.0f, 360.0f));

        Destroy(impact, lifetime);
    }


    ////////////////////////////////////////////////////////////////////////////////
    // ACCESSORS:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public void Sleep() {

        m_JumpHeight = JUMP_HEIGHT_LOW__;
        m_Jumping = false;
        m_Slaming = false;

        m_CurrentHeight = 0.0f;
        m_TargetSpeed = SPEED_HIGH__;
        m_Speed = SPEED_HIGH__;

        m_OutsideOfBounds = false;

        m_PlayerAnimation.m_SpriteRenderer.material.color = Color.white;

        gameObject.SetActive(true);
        CurrentState = State.Sleeping;
    }

    /*----------------------------------------------------------------------------*/
    public void Kill() {
        m_ShadowInstance.SetActive(false);
        gameObject.SetActive(false);
        CurrentState = State.Dead;
    }

    /*----------------------------------------------------------------------------*/
    public bool IsJumping() {
        return m_Jumping;
    }


	//==========================================================================================================================================================================
	//==========================================================================================================================================================================
    // ADDED BY PAT SORRY FOR BAD CODE
	//==========================================================================================================================================================================
	//==========================================================================================================================================================================
	//      ||      
	//      ||
	//      ||
	//      ||
	//      ||
	//   \      /
	//    \    /
	//     \  /
	//      \/

	public void SlamDamage()
	{
		RaycastHit2D[] hit = Physics2D.CircleCastAll(transform.position, 1f, Vector3.up);
		for(int i = 0; i < hit.Length; i++)
		{
			if(hit[i].collider.gameObject.GetComponent<Player>())
			{
				Player player = hit[i].collider.gameObject.GetComponent<Player>();
				if(ID != player.ID)
				{
					player.LoseLife(10, true, "being slammed by someone");
					Vector3 dir = hit[i].collider.gameObject.transform.position - gameObject.transform.position;
					player.m_RigidBody.AddForce(dir*100);
					break;
				}
			}
		}
	}



    public void LoseLife(float _life, bool _canKill, string _whatHitMe)
    {
        GetHurt((int)_life, 0.01f);

		m_Life -= _life;

    	if(m_Life > 1)
    	{
			
			m_BleedCounter -= _life;
    	}


    	if(m_Life <= 0)
    	{
    		if(_canKill)
			{
                SoundManager.SPEAKER.PlaySound("DeathLife");
				Kill();
    		}
    		else   
    		{
				m_Life = 1;
    		}
    	}		

		if(m_BleedCounter <= 0)
		{
			SpawnGoo();
		}
	
		SetSize();
    }

    public void WinLife(float _life)
    {
		m_Life += _life;
		if(m_Life > 100)
		{
			m_Life = 100;
		}
		SetSize();

    }

    void SpawnGoo()
    {
    	if(m_Life != 1)
    	{
			m_GooEmittor.SpawnGoo(transform.position, ID, m_Sprite.color);
			m_BleedCounter += m_BleedCounterInit;
    	}
		
    }

    public void ResetPlayerLife()
    {
        print("life is reset");
		m_Life = 100;
    	m_BleedCounter = m_BleedCounterInit;
		SetSize();
    }

    public void SetSize()
    {
    	
    	if(m_Life != 0)
    	{
			Vector3 scale = transform.localScale;
			float newScale = ( m_Life/m_MaxLife ) * (m_InitScale - m_MinScale);
			newScale = m_MinScale + newScale;

            m_SpeedRatio = 1-(m_Life / m_MaxLife);

			scale.x = newScale;
			scale.y = newScale;
			transform.localScale = scale;
    	}
    }


    public void SetPlayerAnimation()
    {
		m_PlayerAnimation = GetComponentInChildren<PlayerAnimation>();
		m_IsAnimationSet = true;
    }

    public void UpdateAnimation()
    {
		if(m_IsAnimationSet)
		{
			if(m_PlayerAnimation == null)
			{
				m_PlayerAnimation = GetComponentInChildren<PlayerAnimation>();
			}
			else
			{				
				m_PlayerAnimation.m_IsJumping = m_Jumping;
				m_PlayerAnimation.m_IsCharging = m_IsChargingAnimation;
				m_PlayerAnimation.m_IsCharging = m_IsRunningAnimation;
				m_PlayerAnimation.GiveColor(m_Sprite.color);
			}
		}
    }

    private IEnumerator GetHurtCO(int number_of_flashes, float duration_in_between)
    {
        m_GettingHurt = true;
        for (var n = 0; n < number_of_flashes; ++n)
        {
            m_PlayerAnimation.m_SpriteRenderer.material.color = Color.red;
            yield return new WaitForSeconds(duration_in_between);
            m_PlayerAnimation.m_SpriteRenderer.material.color = Color.blue;
            yield return new WaitForSeconds(duration_in_between);
        }

        m_PlayerAnimation.m_SpriteRenderer.material.color = Color.white;
        m_GettingHurt = false;
    }

    public void GetHurt(int number_of_flashes, float duration_in_between)
    {
        StartCoroutine(GetHurtCO(number_of_flashes, duration_in_between));
    }
}