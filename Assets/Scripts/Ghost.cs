﻿using UnityEngine;
using System.Collections;

public class Ghost : MonoBehaviour
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public float m_SpeedFactor = 10.0f;
    private SpriteRenderer m_Renderer;

    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    protected void Start()
    {
        m_Renderer = GetComponent<SpriteRenderer>();
    }

    /*----------------------------------------------------------------------------*/
    protected void Update()
    {
        float alpha = Mathf.Lerp(m_Renderer.color.a, 0, Time.deltaTime * m_SpeedFactor);
        m_Renderer.color = new Color
        (
            m_Renderer.color.r,
            m_Renderer.color.g,
            m_Renderer.color.b,
            alpha
        );
    }
}