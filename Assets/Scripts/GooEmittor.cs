﻿using UnityEngine;
using System.Collections;

public class GooEmittor : MonoBehaviour 
{
	public GameObject m_Goo;



	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void SpawnGoo(Vector3 _pos, int _id, Color _color)
	{
        float x = Random.Range(-1f, 1f);
        float y = Random.Range(-1f, 1f);
        Vector3 d = new Vector3(x, y, 0).normalized;

        GameObject goo = Instantiate(m_Goo, _pos + (d * 0.75f), Quaternion.identity) as GameObject;
        goo.transform.Rotate(0.0f, 0.0f, Random.RandomRange(0.0f, 360.0f));
        goo.GetComponent<GooLeftover>().GiveId(_id, _color);
	}
}
