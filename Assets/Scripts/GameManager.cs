﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using InControl;

public class GameManager : MonoBehaviour
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    private static GameManager INSTANCE__ = null;

    public enum GameState
    {
        None,         // 0) Default state.
        SplashScreen, // 1) Display the dev logo.
        MainMenu,     // 2) Launch the gamplay.
        CharSelect,   // 3) Select each player.
        Game          // 4) Actual gameplay.
    };

    private GameState CurrentState { get; set; }

    // Player related members:
    private const int MAX_PLAYERS__ = 4;
    private List<Player> m_players = new List<Player>(MAX_PLAYERS__);
    public GameObject m_PlayerPrefab;
    bool m_LockPlayersInput = true;

    public int m_NumberOfRound = 4;

    // SplashScreen stage members:
    public GameObject m_SplashScreenObject;
    private SplashScreen m_SplashScreen;

    // Menu stage members:
    public GameObject m_MenuObject;
    private Menu m_Menu;

    // Menu CharacterSelectScreen
    public GameObject m_CharacterSelectScreen;
    private CharacterSelect m_CharacterSelect;

    // Game stage members:
    public GameObject m_HUDObject;
    private HUD m_HUD;

    public GameObject m_ResultScreenObject;
    private ResultScreen m_ResultScreen;

    //for animation
    public GameObject[] m_PlayerAnimationList;


    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    void Awake()
    {
        Debug.Log("Awake GameManager");
        if (INSTANCE__ == null)
        {
            INSTANCE__ = this;
            DontDestroyOnLoad(INSTANCE__.gameObject);
            if (!InitGame())
            {
                Debug.LogError("Unable to init game!");
            }
        }
        else
        {
            Debug.LogError("GameManager already exists!");
        }
    }

    /*----------------------------------------------------------------------------*/
    void Update()
    {
        switch (CurrentState)
        {
            case GameState.SplashScreen:
                if (m_SplashScreen.IsDone()) {
                    SetCurrentState(GameState.MainMenu);
                }

                m_LockPlayersInput = true;
                break;

            case GameState.MainMenu:
                if (m_Menu.IsDone())
                {
                    switch(m_Menu.GetSelectIndex())
                    {
                        case 0:
                            GameObject char1 = Instantiate(m_CharacterSelectScreen) as GameObject;
							m_CharacterSelect = char1.GetComponent<CharacterSelect>();	
							m_CharacterSelect.SetNumOfActivePlayer(GameManager.GetInstance().GetCurrentPlayerCount());
							m_CharacterSelect.CreatePanel();
							CurrentState = GameState.CharSelect;									
                            break;
                        case 1:
                            Application.Quit();
                            break;                 
                    }
                }
                else {
                    CheckForPlayers();
                }

                m_LockPlayersInput = true;
                break;

            case GameState.CharSelect:	
				if(m_CharacterSelect.IsDone())
				{
					SetCurrentState(GameState.Game);	
				}

            	break;

            case GameState.Game:
                if ((GetCurrentPlayerCount() == 1 && GetNumOfPlayersAlive() == 0) ||
                    (GetCurrentPlayerCount() >= 2 && GetNumOfPlayersAlive() == 1))
                {
                    if (!m_ResultScreen.IsShowing())
                    {
                        IncrementPlayerScore();
                        m_LockPlayersInput = true;
                        m_ResultScreen.Show();
                    }
                }
                else {
                    if (m_HUD.IsOpen())
                    {
                        m_LockPlayersInput = true;
                    }
                    else
                    {
                        m_LockPlayersInput = false;
                    }
                }

                if (m_HUD.IsDone() || m_ResultScreen.IsDone()) {
                    SetCurrentState(GameState.MainMenu);
                }
                break;

            default:
                Debug.LogError("Undefined game state!");
                SetCurrentState(GameState.SplashScreen);
                break;
        }
    }

    public void PerformRippleEffect(Vector3 wpos)
    {
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        if (camera)
        {
            Vector3 spos = camera.GetComponent<Camera>().WorldToViewportPoint(wpos);
            RippleEffect fx = camera.GetComponent<RippleEffect>();
            fx.Emit(spos.x, 1-spos.y);
        }
    }

    public void PerformScreenShake(float duration)
    {
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        if (camera)
        {
            CameraShake fx = camera.GetComponent<CameraShake>();
            fx.m_ShakeDuration = duration;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Privates:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    void OnStateEnter(GameState state)
    {
        Debug.Log("OnStateEnter " + state);
        switch (state)
        {
            case GameState.SplashScreen:
                {
                    GameObject o = (GameObject)Instantiate(m_SplashScreenObject);
                    m_SplashScreen = o.GetComponent<SplashScreen>();
                }
                break;

            case GameState.MainMenu:
                {
                    SoundManager.SPEAKER.PlayMusic("Menu");
                    GameObject o = (GameObject)Instantiate(m_MenuObject);
                    m_Menu = o.GetComponent<Menu>();
                }
                break;

            case GameState.Game:
                {
                    SoundManager.SPEAKER.PlayMusic("Game");

                    {
                        GameObject o = (GameObject)Instantiate(m_HUDObject);
                        GameObject.DontDestroyOnLoad(o);
                        m_HUD = o.GetComponent<HUD>();
                    }
                    {
                        GameObject o = (GameObject)Instantiate(m_ResultScreenObject);
                        GameObject.DontDestroyOnLoad(o);
                        m_ResultScreen = o.GetComponent<ResultScreen>();
                    }

                    for (int i = 0; i < m_players.Count; ++i) {
                        Player player = m_players[i];
                        player.Score = 0;
                    }

                    SceneManager.LoadScene(1);
                }
                break;
        }
    }
    /*----------------------------------------------------------------------------*/
    void OnStateLeave(GameState state)
    {
        Debug.Log("OnStateLeave " + state);
        switch (state)
        {
            case GameState.SplashScreen:
                Destroy(m_SplashScreen.gameObject);
                m_SplashScreen = null;
                break;

            case GameState.MainMenu:
                Destroy(m_Menu.gameObject);
                m_Menu = null;
                break;

            case GameState.Game:
                ResetPlayers();
                Destroy(m_HUD.gameObject);
                m_HUD = null;
                break;
        }
    }

    /*----------------------------------------------------------------------------*/
    // Check if an action has been performed by a given input device.
    private bool JoinButtonWasPressedOnDevice(InputDevice input_device)
    {
        return (
            input_device.Action1.WasPressed ||
            input_device.Action2.WasPressed ||
            input_device.Action3.WasPressed ||
            input_device.Action4.WasPressed
        );
    }

    /*----------------------------------------------------------------------------*/
    // Retrieves a player instance from a given input device,
    // returns null if not found.
    private Player FindPlayerUsingDevice(InputDevice input_device)
    {
        for (int i = 0; i < m_players.Count; ++i)
        {
            Player player = m_players[i];
            if (player.Device == input_device)
            {
                return player;
            }
        }

        return null;
    }

    /*----------------------------------------------------------------------------*/
    // Instantiate a new player and assigns the given input device,
    // returns null if too many players are connected.
    private Player CreatePlayer(InputDevice input_device)
    {
        if (m_players.Count < MAX_PLAYERS__)
        {
            Debug.Log("A new player join the game!");

            // instantiate player object
            GameObject player_object = (GameObject)Instantiate(m_PlayerPrefab);
            GameObject.DontDestroyOnLoad(player_object);

            // retrieve player component
            Player component = player_object.GetComponent<Player>();
            component.Device = input_device;
            component.ID = m_players.Count;

            m_players.Add(component);

            return component;
        }

        return null;
    }

    /*----------------------------------------------------------------------------*/
    void CheckForPlayers()
    {
        // get current active device
        InputDevice active_device = InputManager.ActiveDevice;

        // check if device is active
        if (JoinButtonWasPressedOnDevice(active_device))
        {
            if (FindPlayerUsingDevice(active_device) == null)
            {
                CreatePlayer(active_device);
            }
        }
    }

    /*----------------------------------------------------------------------------*/
    bool InitGame()
    {
        CurrentState = GameState.None;
        SetCurrentState(GameState.SplashScreen);
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Accessors:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public static GameManager GetInstance()
    {
        return INSTANCE__;
    }

    /*----------------------------------------------------------------------------*/
    public void SetCurrentState(GameState state)
    {
        if (CurrentState == state)
        {
            return;
        }

        OnStateLeave(CurrentState);
        OnStateEnter(state);
        CurrentState = state;
    }

    /*----------------------------------------------------------------------------*/
    public Player GetPlayerAt(int index)
    {
        return m_players[index];
    }
    /*----------------------------------------------------------------------------*/
    public int GetCurrentPlayerCount()
    {
        return m_players.Count;
    }
    /*----------------------------------------------------------------------------*/
    public static int GetMaxPlayers()
    {
        return MAX_PLAYERS__;
    }
    /*----------------------------------------------------------------------------*/
    public bool isPlayersInputLocked()
    {
        return m_LockPlayersInput;
    }
    /*----------------------------------------------------------------------------*/
    public int GetNumOfPlayersAlive()
    {
        int count = 0;
        for (int i = 0; i < GetCurrentPlayerCount(); ++i) {
            if (GetPlayerAt(i).CurrentState != Entity.State.Dead) {
                count++;
            }
        }

        return count;
    }
    /*----------------------------------------------------------------------------*/
    public void IncrementPlayerScore()
    {
        for (int i = 0; i < GetCurrentPlayerCount(); ++i)
        {
            Player player = GetPlayerAt(i);
            if (player.CurrentState != Entity.State.Dead) {
                player.Score++;
            }
        }
    }
    /*----------------------------------------------------------------------------*/
    public void ResetPlayers()
    {
        for (int i = 0; i < m_players.Count; ++i)
        {
            Player player = m_players[i];
            player.ResetPlayerLife();
            player.Sleep();
        }

        GameObject[] obj = GameObject.FindGameObjectsWithTag("PowerUp");
        for (int i = 0; i < obj.Length; i++) {
            Destroy(obj[i]);
        }
    }

	//==========================================================================================================================================================================
	//==========================================================================================================================================================================
    // ADDED BY PAT SORRY FOR BAD CODE
	//==========================================================================================================================================================================
	//==========================================================================================================================================================================
	//      ||      
	//      ||
	//      ||
	//      ||
	//      ||
	//   \      /
	//    \    /
	//     \  /
	//      \/

	public void GiveAnimationToPlayer(GameObject _playerToGive, int _indexId, Color _color)
	{
		
		if(!_playerToGive.GetComponentInChildren<PlayerAnimation>())
		{
			GameObject animationObj = Instantiate(m_PlayerAnimationList[_indexId], _playerToGive.transform.position, Quaternion.identity) as GameObject;
			animationObj.name = "AnimationObject";
			animationObj.transform.SetParent(_playerToGive.transform);
			animationObj.GetComponent<PlayerAnimation>().GiveColor(_color);
		}
		else
		{
			GameObject animationObj = _playerToGive.transform.FindChild("AnimationObject").gameObject;
			Destroy(animationObj);
			animationObj = Instantiate(m_PlayerAnimationList[_indexId], _playerToGive.transform.position, Quaternion.identity) as GameObject;
			animationObj.name = "AnimationObject";
			animationObj.transform.SetParent(_playerToGive.transform);
			animationObj.GetComponent<PlayerAnimation>().GiveColor(_color);

		}

		
	}

}