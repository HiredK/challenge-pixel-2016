﻿using UnityEngine;
using System.Collections;

public class PuddleController : MonoBehaviour 
{
	public GameObject m_PuddlePrefab;

	private float m_TimerMax = 10f;
	public float m_Timer = 0;

	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(m_Timer >= m_TimerMax)
		{
			CreatePuddle();
			m_Timer = 0;
		}
		else
		{
			m_Timer +=  Time.deltaTime;
		}
	}

	public void CreatePuddle()
	{
		GameObject pubble = Instantiate(m_PuddlePrefab, transform.position, Quaternion.identity) as GameObject;
		pubble.transform.SetParent(transform);
	}

}
