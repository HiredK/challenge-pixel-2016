﻿using UnityEngine;
using System.Collections;

public class GooLeftover : MonoBehaviour 
{
	private int m_PlayerId;
	private bool m_Activated;

	Rigidbody2D m_Rigid;

	public Color m_Color;
	public GameObject m_Sprite;
	private SpriteRenderer m_SpriteRend;

	// Use this for initialization
	void Start () 
	{
		m_Rigid = GetComponent<Rigidbody2D>();
		m_SpriteRend = m_Sprite.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		m_SpriteRend.color = m_Color;
	}

	void OnTriggerEnter2D(Collider2D _col)
	{
		if(_col.gameObject.GetComponent<Player>() && m_Activated)
		{
			

			float playerId = _col.gameObject.GetComponent<Player>().ID;
			Player _player = _col.gameObject.GetComponent<Player>();

			if(_player.m_Jumping || _player.m_Slaming)
			{
				return;
			}
			if(m_PlayerId == playerId)
			{
                SoundManager.SPEAKER.PlaySound("GetGoo");
				_player.WinLife(15);
			}
            else
            {
                SoundManager.SPEAKER.PlaySound("DestroyGoo");
            }

			Destroy(gameObject);
		}
	}

	public void GiveId(int _playerId, Color _color)
	{		
		m_PlayerId = _playerId;	
		m_Color = _color;
		m_Activated = true;
	}


}
