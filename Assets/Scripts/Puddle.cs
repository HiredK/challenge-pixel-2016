﻿using UnityEngine;
using System.Collections;

public class Puddle : MonoBehaviour 
{
	private CircleCollider2D m_Collider;
	private Animator m_Animator;
	private float m_LivingTime = 1.6f;
	private float m_LivingTimer = 0;
	private bool m_TimerDone = false;

	// Use this for initialization
	void Start () 
	{
		m_Collider = GetComponent<CircleCollider2D>();
		m_Animator = GetComponent<Animator>();



	}

	// Update is called once per frame
	void Update () 
	{		
		if(m_LivingTimer >= m_LivingTime)
		{
			m_TimerDone = true;
			Destroy(gameObject);
		}
		else
		{
			m_LivingTimer += Time.deltaTime;
			m_Collider.radius += Time.deltaTime /2.8f;
		}

	
	}

	void OnTriggerStay2D(Collider2D _col)
	{

		if(_col.GetComponent<Player>())
		{
			_col.GetComponent<Player>().LoseLife(1, true, "puddle");
		}

	}

}
