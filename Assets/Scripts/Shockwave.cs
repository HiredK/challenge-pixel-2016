﻿using UnityEngine;
using System.Collections;

public class Shockwave : MonoBehaviour 
{
	public GameObject m_Bullet;
	public GameObject m_BulletBetween;
	public int m_DegreeBetweenNode = 10;	

	private float m_LifeTime = 0;
	private float m_LifeTimeTimer = 0;

	// Use this for initialization
	void Start () 
	{
		Physics.IgnoreLayerCollision(8,8);	
	}
	
	// Update is called once per frame
	void Update () 
	{
		m_LifeTimeTimer += Time.deltaTime;
	}


	/// <summary>
	/// Dos the shockwave.
	/// </summary>
	/// <returns>Return the shockwave position.</returns>
	/// <param name="_player">Player.</param>
	/// <param name="_lifeTime">Life time.</param>
	public Vector3 DoShockwave(GameObject _player, float _lifeTime, Color _color)
	{
		m_LifeTime = _lifeTime*0.8f;

		//Create Halo Object
		GameObject halo = new GameObject();
		halo.name = "halo";
		halo.AddComponent<HaloBullet>();

		//GIve Info To HaloCreator Script
		HaloBullet haloBullet = halo.GetComponent<HaloBullet>();
		haloBullet.m_BulletListNumber = 360/m_DegreeBetweenNode;
		haloBullet.CreateList();
		haloBullet.m_BulletBetweenPrefab = m_BulletBetween;
		haloBullet.m_LifeTime = m_LifeTime;
		haloBullet.UpdatePlayerHitList(_player.GetComponent<Player>().ID);

		for(int i = 0; i < 360; i=i+m_DegreeBetweenNode)
		{			
			float x = Mathf.Cos(i * Mathf.Deg2Rad);
			float y = Mathf.Sin(i * Mathf.Deg2Rad);
			Vector3 dir = new Vector3(x,y,0);
			dir *= 5;

			//set halo starting point to the player creating the shockwave
			halo.transform.position = _player.transform.position;
			GameObject bullet = Instantiate(m_Bullet, _player.transform.position + dir.normalized*.5f, Quaternion.identity) as GameObject;

			//set velocity
			bullet.GetComponent<Rigidbody2D>().velocity = dir;

			//set bullet parent to the halo 
			bullet.transform.parent = halo.transform;		
			haloBullet.m_BulletList[haloBullet.m_BulletListIndex] = bullet;
			haloBullet.m_BulletListIndex++;
		}

		haloBullet.CreateBetween(transform.position, halo, _color);
		return _player.transform.position;
	}
}
