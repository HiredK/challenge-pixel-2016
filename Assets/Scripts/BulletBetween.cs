﻿using UnityEngine;
using System.Collections;

public class BulletBetween : MonoBehaviour 
{
	private GameObject m_HaloParent;
	private HaloBullet m_HaloBullet;
    public Material m_Mat;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 scale = transform.localScale;
		scale.x -= (Time.deltaTime*0.1f)*7;
		if(scale.x < 0)
		{
			scale.x = 0;
		}
		transform.localScale = scale;
	}

	public void SetScale(float _scaleX)
	{
		Vector3 scale = transform.localScale;
		scale.x = _scaleX;
		transform.localScale = scale;
	}

	public void SetHaloParent(GameObject _parent)
	{
		m_HaloParent = _parent;
		m_HaloBullet = m_HaloParent.GetComponent<HaloBullet>();        
	}


	void OnTriggerEnter2D(Collider2D _col)
	{
		//DETECT PLAYER HERE	

		if(_col.GetComponent<Player>())
		{
			Player player = _col.GetComponent<Player>();
			if(m_HaloBullet.m_PlayerHit[player.ID] == false)
			{
				m_HaloBullet.m_PlayerHit[player.ID] = true;
				player.LoseLife(Mathf.Round(transform.localScale.x*50), true, "halo");
			}
		}



	}



}
